// https://vuex.vuejs.org/installation.html
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

const shuffle = array => 
  [...Array(array.length)]
    .map((...args) => Math.floor(Math.random() * (args[1] + 1)))
    .reduce( (a, rv, i) => ([a[i], a[rv]] = [a[rv], a[i]]) && a, array);    

export default new Vuex.Store({
    state: {
        // objects 
        isConnected: false,
        socket: {},
        socket_id: "",
        game: "",
        player: '',
        cards_to_play: [],
        savedUser: {},
        winner: undefined
    },
    getters: {
        is_connected: state => {
            return state.isConnected
        },
        player_id: state => {
            return state.player.id
        },
        cards_to_play: state => {
            return state.cards_to_play
        },
        is_reader: state => {
            return state.player.player_number === state.game.current_player_turn
        },
        is_host: state => {
            return state.player.is_host
        },
        joined_a_game: state => {
            return Object.keys(state.game).length > 1
        },
        need_player_name: state => {
            return state.player.name === ''
        },
        room_code: state => {
            return state.game.game_id.code
        },
        room: state => {
            if (state.game.game_id === ''){
                return shuffle(state.game.gallery)
            }
            return state.game.game_id.uuid
        },
        player: state => {
            return state.player
        },
        players: state => {
            return state.game.players
        },
        black_card: state => {
            return state.game.black
        },
        current_player_turn: state => {
            return state.game.current_player_turn
        },
        gallery: state => {
            if (state.is_reader && state.game.waiting_on.length === 0){
                return state.game.gallery
            }
            return state.game.gallery
        },
        waiting_on: state => {
            return state.game.waiting_on
        },
        winner: state => {
            return state.winner
        }
    },
    mutations: {
        // updates objects
        SAVEUSER(state, payload) {
            let data = {
                'username': payload.username,
                'user_id': payload.user_id,
                'room': payload.room
            }
            localStorage.setItem('singleUser', JSON.stringify(data));
        },
        initializeUser() {
            let user = localStorage.getItem('singleUser')
            if (user != null && Object.keys(user).length !== 0){
                let data = JSON.parse(user)
                this.dispatch('join_a_room', {room: data.room, name: data.username, user_id:data.user_id})
            }
        },
        SOCKET_CONNECT(state, payload) {
            state.isConnected = true;
            state.socket_id = payload.id
            state.socket = payload
        },      
        SOCKET_DISCONNECT(state) {
            state.isConnected = false;
        },
        SOCKET_JOINED(state, payload) {
            console.log('Joined:', payload)
        },
        SOCKET_INIT(state, payload) {
            console.log('init:', payload)
        },
        SOCKET_UPDATE_GAME(state, payload) {
            state.game = payload
        },
        SOCKET_UPDATE_PLAYER(state,payload){
            state.player = payload
        },
        SOCKET_NEW_PLAYER(state, payload) {
            state.game.players.push(payload)
        },
        SOCKET_NEW_ROUND(state) {
            state.cards_to_play = []
        },
        ENDGAME(state){
            localStorage.removeItem('singleUser');
            state.game = ''
            state.player = ''
            state.cards_to_play = []
            state.savedUser = ''
        },
        LEAVE_GAME(state){
            localStorage.removeItem('singleUser');
            state.game = ''
            state.player = ''
            state.cards_to_play = []
            state.savedUser = ''
        },
        IWON(state) {
            state.winner = true
        },
        ILOST(state) {
            state.winner = false
        },
        WINNER_RESET(state) {
            state.winner = undefined
        }
    },
    actions: {
        // pulls data and uses mutations 
        InitializeUser({commit}){
            commit('initializeUser')
        },
        endGame({commit, state, getters}) {
            state.socket.emit('end_game', {room: getters.room})
            commit("ENDGAME")
        },
        leaveGame({commit, state, getters}) {
            state.socket.emit('leave_game', {room: getters.room, player: getters.player})
            commit('LEAVE_GAME')
        },
        connect({commit}, payload) {
            commit('SOCKET_CONNECT', payload)
        },
        create_a_room({state}, payload) {
            state.socket.emit('start_a_game', payload)
        },
        join_a_room({state}, payload) {
            state.socket.emit('join_a_game', payload)
        },
        update_game({commit}, payload) {
            commit('SOCKET_UPDATE_GAME', payload)
        },
        update_player({commit, getters}, payload) {
            if (getters.room != '' && payload.name != ''){
                commit('SAVEUSER', {
                    'username': payload.name,
                    'user_id': payload.id,
                    'room': getters.room
                })
            }            
            commit('SOCKET_UPDATE_PLAYER', payload)
        },
        new_player({commit}, payload) {
            commit('SOCKET_NEW_PLAYER', payload)
        },
        card_played({state}, payload) {
            state.socket.emit('card-played', payload)
        },
        reader_flipped({state}, payload) {
            state.socket.emit('reader_flipped', payload)
        },
        update_player_name({ state, getters }, payload) {
            state.socket.emit('update-player-name', {
                player: payload,
                room: getters.room
            })
        },
        new_round({commit}) {
            commit('SOCKET_NEW_ROUND')
            commit('WINNER_RESET')
        },
        winner_selected({state, getters}, payload) {
            state.socket.emit('winner-selected', {
                room: getters.room,
                id: payload
            })
        },
        winner({commit, getters}, playload) {
            if (getters.is_reader) {
                return 
            }
            if (getters.player.id == playload.id) {
                commit('IWON')
                this._vm.$confetti.start({
                    particles: [{type: 'heart',}],
                    defaultColors: ['#00FOA8','#00A890','#309039', '#30FF60', '00C000'],
                })
                setTimeout(() => {
                    this._vm.$confetti.stop();
                }, 2000)
            } else {
                commit('ILOST')
                this._vm.$confetti.start({
                    particles: [{type: 'heart',}],
                    defaultColors: ['red','pink','#ba0000',],
                })
                setTimeout(() => {
                    this._vm.$confetti.stop();
                }, 2000)                
            }
        }
    }
})