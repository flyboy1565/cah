import Vue from 'vue'
import App from './App.vue'
import store from './store'


import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

// https://www.npmjs.com/package/vue-cookies
import VueCookies from 'vue-cookies'
Vue.use(VueCookies)


// https://alligator.io/vuejs/vue-socketio/
import io from 'socket.io-client';
import VueSocketIOExt from 'vue-socket.io-extended';

// const NGROK_URL = "cadadc474557.ngrok.io"
// const URL = window.location.protocol + NGROK_URL
const URL = 'https://5bdd7f6cf6d0.ngrok.io'
export default URL
const socket = io(URL);
Vue.use(VueSocketIOExt, socket, store );


// https://github.com/alexandermendes/vue-confetti
import VueConfetti from 'vue-confetti'

Vue.use(VueConfetti)

Vue.config.devtools = true;


new Vue({
  store,
  render: h => h(App),
}).$mount('#app')
