export default class Deck {
    constructor(all_cards) {
        this.all_cards = all_cards
        this.all_black_cards = all_cards['blackCards']
        this.all_white_cards = all_cards['whiteCards']
        this.deck_short_names = all_cards['order']
        this.cards = this.build_deck()
        this.discards = []
    }

    shuffle(array) {
        return array.sort(() => Math.random() - 0.5);
    }
    
    build_deck () {
        let cards = {
            black: [],
            white: [],
        }
        while (cards.black.length < 30){
            var deck = this.deck_short_names[Math.floor(Math.random()*this.deck_short_names.length)]
            var deck_full = this.all_cards[deck].name
            var white = this.all_cards[deck].white
            var black = this.all_cards[deck].black
            var all_white_cards = this.all_white_cards
            var all_black_cards = this.all_black_cards
            white.forEach(function(item){
                var card = {
                    text: all_white_cards[item], 
                    id:'w-'+ item,
                    deck: deck,
                    deck_name: deck_full
            }
                cards.white.push(card)
            })
            black.forEach(function(item){
                var card = {
                    text: all_black_cards[item]['text'], 
                    id:'b-' + item,
                    blank: all_black_cards[item]['blank'], 
                    deck: deck,
                    deck_name: deck_full
                }
                cards.black.push(card)
            })
        }
        cards.white = this.shuffle(cards.white)
        cards.black = this.shuffle(cards.black)
        return cards
    }
}