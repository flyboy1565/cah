// https://vuex.vuejs.org/installation.html
import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

new Vuex.Store({
    state: {
        // objects 
    },
    getters: {
        // getting data
    },
    actions: {
        // pulls data and uses mutations 
    },
    mutations: {
        // updates objects
    }
})