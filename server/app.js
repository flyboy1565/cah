import { createRequire } from 'module';
const require = createRequire(import.meta.url);

import Game from './game.js'
import { stringify } from 'querystring';
import { Console } from 'console';
import { resolve } from 'path';

const app = require('express')();
const server = require('http').createServer(app);
const SocketIo = require('socket.io')(server, { serveClient: false });

const games = []


const wait = async (time=1000) =>  new Promise(resolve => setTimeout(resolve, time));

async function botFlip (game) {
    while (game.gallery.filter(p=> !p.flipped).length > 0){
        game.bot_random_flip()
        SocketIo.sockets.to(game.game_id.uuid).emit('game_update', {game:game})
        await wait(3000)
    }
    await wait(3000)
    let winner = game.bot_random_winner()
    await wait(2000)
    setWinner({room:game.game_id.uuid, id:winner.id})
}

async function setWinner(obj) {
    let game = getGame(obj.room)
    let player = game.get_player(obj.id)
    // console.log('Player: ', player)
    game.select_winner(player.id)
    await wait(2000)
    SocketIo.sockets.to(game.game_id.uuid).emit('winner-winner',player)
    await wait(2000)
    SocketIo.sockets.to(game.game_id.uuid).emit('game_update', {game:game})
    SocketIo.sockets.to(game.game_id.uuid).emit('new_round')
}

function listGameRoomsCodes(){
    return games.map((i)=> i.game_id.code)
}
function listGameRooms(){
    return games.map((i)=> i.game_id.uuid)
}
function listPlayers(game){
    return game.players.filter((i)=> (i.name, i.id))
}
function getGame (room) {
    let game = games.find(g => g.game_id.uuid == room || g.game_id.code == room)
    return game
}
function getPlayer(room, id) {
    let game = getGame(room)
    let player = game.players.find(p => p.id === id)
    return [player, game]
}

SocketIo.on("connection", socket => {

    socket.emit('connect', socket.id)

    socket.on('check_exiting_game', obj => {
        let game = getGame(obj.code)
        if (game != undefined) {
            socket.emit('game_exists', {exists: true})
        } else {
            socket.emit('game_exists', {exists: false})
        }
    })

    socket.on('get_code', obj => {
        let game = getGame(obj.room)
        if (game){
            socket.emit('room_code', game.get_code())
        }
    })
      
    socket.on('start_a_game', function (name) {
        let game = new Game()
        let player = game.add_player(socket.id)
        socket.join(game.game_id.uuid)
        player.is_host = true
        game.get_white_cards(player)
        game.get_black_card()
        player.update_name(name.name)
        games.push(game)
        socket.emit('game_update', {game: game, player: player})
    })

    socket.on('join_a_game', obj => {
        let game = getGame(obj.room)
        if (game === undefined) { return }
        let player = game.players.find(p=> p.id == obj.user_id)
        if (player) {
            player.id = socket.id
        } else {
            player = game.add_player(socket.id)
            if (!player){
                socket.emit('room_full')   
                return      
            }
        }
        socket.join(game.game_id.uuid)
        game.get_white_cards(player)
        player.update_name(obj.name)
        socket.emit('game_update', {game: game, player: player})
        SocketIo.sockets.to(game.game_id.uuid).emit('game_update', {game:game})
    })

    
    socket.on('spawn_bot', obj => {
        let game = getGame(obj.room)
        let bots = game.players.filter(i=> i.is_bot === true)
        if (bots.length < 5){
            let name = "Bot" + (bots.length + 1)
            let player = game.add_player(name)
            player.update_name(name)
            player.is_bot = true
            player.id = game.game_id.uuid + '-' + name
            game.get_white_cards(player)
            let obj = player.bot_play_card(game.game_id.uuid, game.black.blank_count)
            game.waiting_on = game.waiting_on.filter(p=> p.id !== player.id)
            game.gallery.push(obj)
            SocketIo.sockets.to(game.game_id.uuid).emit('game_update', {game:game})
        }
    })
    
    socket.on('leave_game', obj => {
        let game = getGame(obj.room)
        let player = game.get_player(obj.player.id)
        if (player){
            let index = game.players.findIndex(p => p.id === obj.player.id)
            game.players.splice(index, 1)
            SocketIo.sockets.to(game.game_id.uuid).emit('game_update', {game:game})
        }
    })
    
    socket.on('update-player-name', obj => {
        let game = getGame(obj.room)
        let player = game.get_player(obj.player.id)
        player.update_name(obj.player.name)
        socket.to(game.game_id.uuid).emit('update-player-name', game)
    })

    socket.on('end_game', obj => {
        let index = getGame(obj.room)
        games.splice(index,1)
        SocketIo.sockets.to(index.game_id.uuid).emit('game_update', {game:''})        
    })

    socket.on('reader_flipped', obj => {
        let game = getGame(obj.room)
        let flippedCard = game.gallery.find(p => p.id === obj.id)
        if (flippedCard !== undefined){
            flippedCard.flipped=true
            game.logger('A card was flipped')
            SocketIo.sockets.to(game.game_id.uuid).emit('game_update', {game:game})
        }
    })

    socket.on('card-played', obj => {
        let [player, game] = getPlayer(obj.room, obj.player.id)
        obj.cards.forEach(card => {
            player.hand = player.hand.filter(c => c.id !== card.id)
        })
        game.waiting_on = game.waiting_on.filter(p=> p.id !== obj.player.id)
        game.gallery.push(obj)
        game.gallery = game.shuffle(game.gallery)
        console.log('Player:', player.name, "played a card")
        console.log('Check for Bot: ', (game.waiting_on.filter(p => p.is_bot).length > 0))
        if (game.waiting_on.filter(p => p.is_bot).length > 0){
            game.waiting_on.filter(p => p.is_bot).forEach(bot => {
                console.log('Bot: ', bot.name, bot.player_number)
                if (bot.player_number !== game.current_player_turn){
                    let obj = bot.bot_play_card(game.game_id.uuid, game.black.blank_count)
                    game.waiting_on = game.waiting_on.filter(p=> p.id !== bot.id)
                    game.gallery.push(obj)
                }
            })
        }
        socket.emit('game_update', {game: game, player: player})
        SocketIo.sockets.to(game.game_id.uuid).emit('game_update', {game:game})
        let bot = game.players.find(p=> p.player_number === game.current_player_turn && p.is_bot)
        if (bot !== undefined){
            console.log('BOT:', bot)
            if (bot.is_bot && game.waiting_on.length === 0) {
                console.log("Bot is the reader and we're not waiting on other players")
                // Todo fix promises
                botFlip(game)
                 
            }
        }
    })

    socket.on('winner-selected', obj => {
        setWinner(obj)
    })

    socket.on('disconnect', () =>{
        // console.log("disconnected:", socket.id)
    })
})

server.listen(3000, () => {
    console.log('listening on 3000..')

})