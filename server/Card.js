export default class Card {
    constructor(id, color, text, deck, deck_full, blank=null) {
        this.color = color
        this.text = text, 
        this.deck =  deck,
        this.deck_name = deck_full
        this.id = this.generate_id(id)
        this.blank_count = blank
        this.used = false
    }
    generate_id (number) {
        if (this.color === 'black') {
            return 'b-'+ number
        }
        return 'w-'+ number
    }
}