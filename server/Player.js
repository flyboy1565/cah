class Player {
    constructor (id, player_number) {
        this.id = id
        this.player_number = player_number
        this.name = ''
        this.hand = []
        this.cards_won = []
        this.is_host = false
        this.is_bot = false
    }

    update_name (name) {
        this.name = name
    }

    random_card () {
        var card = this.hand[Math.floor(Math.random() * this.hand.length)]
        return card 
    }

    bot_play_card (room, blank_count) {
        let counter = 0
        let cards = []
        console.log('bot ', this.name, 'is playing a card')
        while (counter < blank_count){    
            console.log(counter)
            let new_card = this.random_card()
            cards.push(new_card)
            counter++
            console.log(counter)
        }
        let obj = {cards: cards, player:this, flipped:false, room:room, id:this.id}
        // console.log('OBJ:', obj)
        return obj
    }
    
    // used_card(id) {
    //     this.hand.filter(c => c.)
    // }
}    

export default Player