import Player from './Player.js'
import Deck from './Deck.js'

import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const CAHJson = require('./cards.json');

export default class Game {
  constructor() {
    this.game_id =  {
      uuid: this.generate_id(), 
      code: this.generate_code()
    }
    this.players = []
    this.gallery = []
    this.waiting_on = []
    this.log = []
    this.deck = new Deck(CAHJson)
    this.black = this.deck.draw_black()
    this.current_player_turn = 1
  }
  generate_id () {
    return Math.floor(Math.random() * Date.now())
  }
  generate_code () {
    let number = Math.floor(Math.random()* Date.now())
    return number.toString().substring(0, 4);
  }
  logger (message) {
    this.log.push({
      timestamp: Date.now(),
      message: message
    })
  }
  add_player(id) {
    let number = this.players.length + 1
    if (this.players.length + 1 < 7) {
      let player = new Player(id, number)
      this.logger("Player " + number + " joined the room")
      this.players.push(player)
      if (player.player_number !== this.current_player_turn){
        this.waiting_on.push(player)
      }
      return player
    } else {
      return false
    }
  }
  shuffle(array) {
    return array.sort(() => Math.random() - 0.8);
  }
  get_player(id){
    return this.players.find(p => p.id === id)
  }
  remove_player(id) {
    let player = this.players.find(p => p.id === id)
    this.logger(`Player ${player.play_number} left the room`)
    this.players = this.players.filter(p => p.id !== id)
  }
  get_black_card() {
    let card = this.deck.draw_black()
    return card
  }
  get_white_cards(player) {
    while (player.hand.length < 6){
      let card = this.deck.draw_white(1)
      player.hand.push(card)
    }
    // console.log(`${player.name} now has ${player.hand.length}`)
  }
  new_round() {
    console.log('Setting up a new round')
    this.current_player_turn += 1
    if (this.current_player_turn > this.players.length){
      this.current_player_turn = 1
    }
    this.black = this.get_black_card()
    this.gallery = []
    this.waiting_on = this.players.filter(p=> p.player_number !== this.current_player_turn )
    this.players.filter(p => p.is_bot).forEach(bot => {
      console.log('BotPlayer: ', bot.name)
      if (bot.player_number !== this.current_player_turn){
        let obj = bot.bot_play_card(this.game_id.uuid, this.black.blank_count)
        this.waiting_on = this.waiting_on.filter(p => p.id !== bot.id)
        this.gallery.push(obj)
      }
    })
    this.players.forEach(player => {
      // console.log(`${player.name} has ${player.hand.length} cards.`)
      this.get_white_cards(player)
    })
    console.log('End round setup.')
  }
  select_winner(player_id) {
    console.log('Player Id: ', player_id)
    let player = this.players.find(p => player_id === p.id)
    console.log('Player: ', player.name)
    this.logger("Player " + this.current_player_turn + " awarded a black card to Player ", player.play_number)
    player.cards_won.push(this.black)
    this.new_round()
  }
  get_code(){
    return this.game_id.code
  }
  bot_random_winner(){
    // var card = this.hand[Math.floor(Math.random() * this.hand.length)]
    var winner = this.gallery[Math.floor(Math.random() * this.gallery.length)]
    return winner
  }
  bot_random_flip(){
    var player_set = this.gallery[Math.floor(Math.random() * this.gallery.length)]
    if (player_set.flipped) {
      return false
    }
    player_set.flipped = true
    return true
  }

}

