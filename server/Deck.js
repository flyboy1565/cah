import Card from './Card.js'

export default class Deck {
    constructor(all_cards) {
        this.all_cards = all_cards
        this.all_black_cards = all_cards['blackCards']
        this.all_white_cards = all_cards['whiteCards']
        this.deck_short_names = all_cards['order']
        this.cards ={black: [], white: []}
        this.discards = []
        this.build_deck()
    }

    shuffle(array) {
        return array.sort(() => Math.random() - 0.5);
    }
    
    build_deck () {
        let cards = {
            black: [],
            white: [],
        }
        while (cards.black.length < 30){
            var deck = this.deck_short_names[Math.floor(Math.random()*this.deck_short_names.length)]
            var deck_full = this.all_cards[deck].name
            var white = this.all_cards[deck].white
            var black = this.all_cards[deck].black
            var all_white_cards = this.all_white_cards
            var all_black_cards = this.all_black_cards
            white.forEach(function(item){
                let text = all_white_cards[item]
                let card = new Card(item, 'white', text, deck, deck_full)
                cards.white.push(card)
            })
            black.forEach(function(item){
                let text = all_black_cards[item]['text']
                let blank = all_black_cards[item]['blank']
                let card = new Card(item, 'black', text, deck, deck_full, blank)
                cards.black.push(card)
            })
        }
        this.cards.white = this.shuffle(cards.white)
        this.cards.black = this.shuffle(cards.black)
    }

    draw_black() {
        var black = this.cards.black 
        var card = black[Math.floor(Math.random() * black.length)]
        this.cards.black = black.filter(p => p.id !== card.id)
        return card
    }

    draw_card () {
        let white = this.cards.white
        let card = white[Math.floor(Math.random() * white.length)]
        this.cards.white = white.filter(p => p.id !== card.id)
        return card
    }

    draw_white(count) {
        var white = this.cards.white.filter(c => c.used === false)
        var card = white[Math.floor(Math.random() * white.length)]
        card.used=true
        return card
    }
}